#include <stdint.h>
#include <bigi.h>
#include <bigi_io.h>
#include <ecc.h>
#include <rng.h>

#ifndef __MIKROC_PRO_FOR_ARM__
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#endif

// #define MONT_PART
#define EXAMPLE_BITLEN 239
#define EXAMPLE_WORDLEN ((EXAMPLE_BITLEN + MACHINE_WORD_BITLEN - 1) / (MACHINE_WORD_BITLEN))
#define EXAMPLE_TMPARY_LEN 20

#ifdef __MIKROC_PRO_FOR_ARM__
char received_x_A_str   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
char received_y_A_str   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t rand_kB_ary      [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t received_x_A_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)]; 
word_t received_y_A_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)]; 
word_t computed_x_B_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)]; 
word_t computed_y_B_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)]; 
word_t p_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t a_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t b_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_P_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_P_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

word_t O_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

word_t x_out_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_out_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

#ifdef MONT_PART
// word_t p_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t a_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t b_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_P_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_P_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_Q_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_Q_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t r_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_out_mont_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_out_mont_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t mu_mont = 0;
#endif /*MONT_PART*/

word_t tmp_arys[EXAMPLE_TMPARY_LEN * (EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
bigi_t tmpary[EXAMPLE_TMPARY_LEN];
#endif /* ifdef __MIKROC_PRO_FOR_ARM__ */

#ifdef __MIKROC_PRO_FOR_ARM__
    #define PRINTF(BUFFER) UART0_Write_Text(BUFFER)
    #define CLOCKS_PER_SEC 48000 /* TODO: check */
    volatile uint64_t ISR_COUNT = 0;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    #define PRINTF(BUFFER) printf("%s", BUFFER)
    clock_t t_start;
#endif /* ifdef __MIKROC_PRO_FOR_ARM__ */

#ifdef DBGT
    #define TIMERCALL(func) { \
                                TimerStart(); \
                                BIGI_ST_VAR = func; \
                                ticks = TimerGet(); \
                                if (BIGI_ST_VAR != OK) \
                                { \
                                    PRINTF("\nraise \"Error code: "); \
                                    PRINTF(bigi_status_str(BIGI_ST_VAR)); \
                                    PRINTF("\"\n# "); \
                                } \
                            }
#else
    #define TIMERCALL(func) { \
                                TimerStart(); \
                                func; \
                                ticks = TimerGet(); \
                            }
#endif

void TimerStart()
{
#ifdef __MIKROC_PRO_FOR_ARM__
    word_t i;

    ISR_COUNT = 0;
    EC_REG_BANK_INST_INTERRUPT_CONTROL &= ~1l;
    INTS_INST_GIRQ23_SRC = 16;
    INTS_INST_GIRQ23_EN_CLR = 16;
    INTS_INST_GIRQ23_EN_SET = 16;
    INTS_INST_BLOCK_ENABLE_SET = 1l << 23;
    EnableInterrupts();
    NVIC_IntEnable(IVT_INT_GIRQ23);
    TIMER4_INST_CONTROL = 0x10;

    for(i = 0; i < 100; i++) asm nop; // soft delay

    TIMER4_INST_PRE_LOAD = 48000;
    TIMER4_INST_INT_EN  = 1;
    TIMER4_INST_CONTROL = 0x00000029;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    t_start = clock();
#endif
}

uint64_t TimerGet()
{
    uint64_t t_res;

#ifdef __MIKROC_PRO_FOR_ARM__
    DisableInterrupts();
    t_res = ISR_COUNT;
    ISR_COUNT = 0;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    clock_t t_end;
    t_end = clock();
    t_res = (uint64_t)(t_end - t_start);
#endif

    return t_res;
}

#ifdef __MIKROC_PRO_FOR_ARM__
void TimerInterrupt() iv IVT_INT_GIRQ23 ics ICS_AUTO
{
    if (INTS_INST_GIRQ23_EN_SET & (1ul << 4))
    {
        INTS_INST_GIRQ23_SRC |= 1ul << 4;
        ISR_COUNT++;
    }
}
#endif

// #ifdef __MIKROC_PRO_FOR_ARM__
// char getch(void)
// {
//     return UART0_Read();
// }
// #endif

#ifdef __MIKROC_PRO_FOR_ARM__
void main()
#else
int main(int argc, char ** argv)
#endif
{
    /* values from https://www.certicom.com/content/dam/certicom/images/pdfs/challenge-2009.pdf */
#if EXAMPLE_BITLEN == 79
    char * p   = "000062CE5177412ACA899CF5";
    char * a   = "000039C95E6DDDB1BC45733C";
    char * b   = "00001F16D880E89D5A1C0ED1";

    char * x_P = "0000315D4B201C208475057D";
    char * y_P = "0000035F3DF5AB370252450A";
#elif EXAMPLE_BITLEN == 239
    char * p   = "00007CFB4C973A86CDAF898231E4960ACDBBF5B6A9017DBED75FFABDD892085D";
    char * a   = "000076D4219CF7498B5B471E85BC4DABA3CE47ADC806228FBB0BCE197C4F4556";
    char * b   = "00004F0911A649B98CD0D3F695695E44743EA948E70B78CAB2C24C4E7D50E2B3";

    char * x_P = "00000D35ED464403B23CC681F18534C14B6FA2ADE7720523F5094AD9BFBE4752";
    char * y_P = "000052F1BC7C3C7438A91099FDD53666A0185FB59688CA3E380840903B589BEB";
#else
    #error "Unknown example for given bigi bit-length."
#endif

    /* declare variables */
    uint64_t ticks;
#define BUFFLEN ((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN + 3) * HEXCHARS_PER_MW)
    char buff[BUFFLEN];

    index_t i;
    DECL_BIGI_STATUS

#ifndef __MIKROC_PRO_FOR_ARM__
    char *const received_x_A_str = malloc((EXAMPLE_WORDLEN * sizeof(word_t) * 2) + 1);
    char *const received_y_A_str = malloc((EXAMPLE_WORDLEN * sizeof(word_t) * 2) + 1);

    word_t *const rand_kB_ary    = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const received_x_A_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const received_y_A_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const computed_x_B_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const computed_y_B_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const p_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const a_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const b_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const x_P_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_P_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const O_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const x_out_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_out_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

#ifdef MONT_PART
    // word_t *const p_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const a_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const b_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_P_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_P_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_Q_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_Q_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const r_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_out_mont_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_out_mont_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t mu_mont = 0;
#endif /*MONT_PART*/

    bigi_t *const tmpary  = malloc(EXAMPLE_TMPARY_LEN * sizeof(bigi_t));
#endif /* #ifndef __MIKROC_PRO_FOR_ARM__ */

    bigi_t rand_kB_bigi, received_x_A_bigi, received_y_A_bigi, computed_x_B_bigi, computed_y_B_bigi;

    bigi_t p_bigi, a_bigi, b_bigi, x_P_bigi, y_P_bigi;

    bigi_t x_O_bigi, y_O_bigi;

    bigi_t x_out_bigi, y_out_bigi;
#ifdef MONT_PART
    bigi_t a_mont_bigi, b_mont_bigi, x_P_mont_bigi, y_P_mont_bigi, x_Q_mont_bigi, y_Q_mont_bigi, r_mont_bigi;
    bigi_t x_out_mont_bigi, y_out_mont_bigi;
#endif /*MONT_PART*/

#ifdef __MIKROC_PRO_FOR_ARM__
    UART0_Init(115200);
    Delay_ms(100);   // Wait for UART module to stabilize

    PRINTF("\n# -------\n");
#endif

    rand_kB_bigi.ary = rand_kB_ary;
    rand_kB_bigi.wordlen = EXAMPLE_WORDLEN;
    rand_kB_bigi.domain = DOMAIN_NORMAL;

    received_x_A_bigi.ary = received_x_A_ary;
    received_x_A_bigi.wordlen = EXAMPLE_WORDLEN;
    received_x_A_bigi.domain = DOMAIN_NORMAL;

    received_y_A_bigi.ary = received_y_A_ary;
    received_y_A_bigi.wordlen = EXAMPLE_WORDLEN;
    received_y_A_bigi.domain = DOMAIN_NORMAL;

    computed_x_B_bigi.ary = computed_x_B_ary;
    computed_x_B_bigi.wordlen = EXAMPLE_WORDLEN;
    computed_x_B_bigi.domain = DOMAIN_NORMAL;

    computed_y_B_bigi.ary = computed_y_B_ary;
    computed_y_B_bigi.wordlen = EXAMPLE_WORDLEN;
    computed_y_B_bigi.domain = DOMAIN_NORMAL;

    p_bigi.ary = p_ary;
    p_bigi.wordlen = EXAMPLE_WORDLEN;
    p_bigi.domain = DOMAIN_NORMAL;

    a_bigi.ary = a_ary;
    a_bigi.wordlen = EXAMPLE_WORDLEN;
    a_bigi.domain = DOMAIN_NORMAL;

    b_bigi.ary = b_ary;
    b_bigi.wordlen = EXAMPLE_WORDLEN;
    b_bigi.domain = DOMAIN_NORMAL;

    x_P_bigi.ary = x_P_ary;
    x_P_bigi.wordlen = EXAMPLE_WORDLEN;
    x_P_bigi.domain = DOMAIN_NORMAL;

    y_P_bigi.ary = y_P_ary;
    y_P_bigi.wordlen = EXAMPLE_WORDLEN;
    y_P_bigi.domain = DOMAIN_NORMAL;

    x_O_bigi.ary = O_ary;
    x_O_bigi.wordlen = EXAMPLE_WORDLEN;
    x_O_bigi.domain = DOMAIN_NORMAL;

    y_O_bigi.ary = O_ary;
    y_O_bigi.wordlen = EXAMPLE_WORDLEN;
    y_O_bigi.domain = DOMAIN_NORMAL;

    x_out_bigi.ary = x_out_ary;
    x_out_bigi.wordlen = EXAMPLE_WORDLEN;
    x_out_bigi.domain = DOMAIN_NORMAL;

    y_out_bigi.ary = y_out_ary;
    y_out_bigi.wordlen = EXAMPLE_WORDLEN;
    y_out_bigi.domain = DOMAIN_NORMAL;

#ifdef MONT_PART

    a_mont_bigi.ary = a_mont_ary;
    a_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    a_mont_bigi.domain = DOMAIN_MONTGOMERY;

    b_mont_bigi.ary = b_mont_ary;
    b_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    b_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_P_mont_bigi.ary = x_P_mont_ary;
    x_P_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_P_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_P_mont_bigi.ary = y_P_mont_ary;
    y_P_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_P_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_Q_mont_bigi.ary = x_Q_mont_ary;
    x_Q_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_Q_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_Q_mont_bigi.ary = y_Q_mont_ary;
    y_Q_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_Q_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_out_mont_bigi.ary = x_out_mont_ary;
    x_out_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_out_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_out_mont_bigi.ary = y_out_mont_ary;
    y_out_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_out_mont_bigi.domain = DOMAIN_MONTGOMERY;

    r_mont_bigi.ary = r_mont_ary;
    r_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    r_mont_bigi.domain = DOMAIN_NORMAL;
#endif /*MONT_PART*/

    for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
    {
#ifdef __MIKROC_PRO_FOR_ARM__
        tmpary[i].ary      = &tmp_arys[i * (EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
#else
        tmpary[i].ary      = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
#endif
        tmpary[i].wordlen  = EXAMPLE_WORDLEN;
        tmpary[i].alloclen = EXAMPLE_WORDLEN;
        tmpary[i].domain   = DOMAIN_NORMAL;
    }

#ifdef DBGT
    PRINTF("---   DEBUG / TEST mode ON                            ---\n");
#else
    PRINTF("---   DEBUG / TEST mode OFF                            ---\n");
#endif
    sprintf(buff, "--    %u-bit numbers\n", EXAMPLE_BITLEN);
    PRINTF(buff);

    sprintf(buff, "--    %u-word numbers (alligned)\n", EXAMPLE_WORDLEN);
    PRINTF(buff);

    sprintf(buff, "--    %u-bit machine\n", MACHINE_WORD_BITLEN);
    PRINTF(buff);

    /* load from hex */
    PRINTF("# Loading numbers ...\n\n");

    TIMERCALL(bigi_from_hex(p, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &p_bigi))
    PRINTF("#  - loaded p = \n");
    bigi_print(&p_bigi);
    PRINTF("\n");
    TIMERCALL(bigi_from_hex(a, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &a_bigi))
    PRINTF("#  - loaded a\n");
    TIMERCALL(bigi_from_hex(b, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &b_bigi))
    PRINTF("#  - loaded b\n");

    TIMERCALL(bigi_from_hex(x_P, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &x_P_bigi))
    PRINTF("#  - loaded x_P\n");
    TIMERCALL(bigi_from_hex(y_P, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &y_P_bigi))
    PRINTF("#  - loaded y_P\n");
    /* set point in infinity as O = [0, 0]*/
    TIMERCALL(bigi_set_zero(&x_O_bigi))
    TIMERCALL(bigi_set_zero(&y_O_bigi))
    PRINTF("#  - set  O = [0, 0]\n");
    /* -------------------------------------------------------------------------
     *  Init RNG - not needed now
     *                                                                        */
    rng_init();
    PRINTF("\n\n# Set random kB ...\n");
    rng_get_bigi(&rand_kB_bigi, rand_kB_bigi.wordlen);
    PRINTF("#  - kB = 0x");
    bigi_print(&rand_kB_bigi);
    PRINTF("\n");
    i = (rand_kB_bigi.wordlen * MACHINE_WORD_BITLEN)  + (MULT_BUFFER_WORDLEN * MACHINE_WORD_BITLEN) - EXAMPLE_BITLEN;
    bigi_shift_left(&rand_kB_bigi, i);
    bigi_shift_right(&rand_kB_bigi, i);      
    PRINTF("#  - kB with correct bit lenght= 0x");
    bigi_print(&rand_kB_bigi);
    PRINTF("\n\n");
/* ########################################################################## */
    /* -------------------------------------------------------------------------
     *  compute B = kB*P
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("compute B = kB*P:\n");
    TIMERCALL(ecc_mult(&p_bigi, &a_bigi, &b_bigi, &rand_kB_bigi, &x_P_bigi, &y_P_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &computed_x_B_bigi, &computed_y_B_bigi))
    PRINTF(" - computed_x_B_bigi = 0x");
    bigi_print(&computed_x_B_bigi);
    PRINTF("\n - computed_y_B_bigi = 0x");
    bigi_print(&computed_y_B_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

/* ########################################################################## */
    /* -------------------------------------------------------------------------
     *  receive A from Alice
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF(">>> ready to receive x_A:\n");
#ifdef __MIKROC_PRO_FOR_ARM__
    for (i = 0; i < ((EXAMPLE_WORDLEN * sizeof(word_t) * 2)); ++i) {
        received_x_A_str[i] = UART0_Read();
    }
    BIGI_CALL(bigi_from_hex(received_x_A_str, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &received_x_A_bigi))
    PRINTF("- received x_A = 0x");
    bigi_print(&received_x_A_bigi);
    PRINTF("\n\n");
#else
    if ( NULL != fgets(received_x_A_str, (EXAMPLE_WORDLEN * sizeof(word_t) * 2) + 1, stdin)) {
        TIMERCALL(bigi_from_hex(received_x_A_str, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &received_x_A_bigi))
        PRINTF("- received x_A = 0x");
        bigi_print(&received_x_A_bigi);
        PRINTF("\n\n");
    }
    else {
        PRINTF("- ERROR OCCURED WHILE ENTERING THE NUMBER!\n\n");
    }
    i = scanf("%*[^\n]"); // flush the input stream
    i = 0;
    getchar(); // consume the newline character left in the input stream
#endif

    PRINTF(">>> ready to receive y_A:\n");
#ifdef __MIKROC_PRO_FOR_ARM__
    for (i = 0; i < ((EXAMPLE_WORDLEN * sizeof(word_t) * 2)); ++i) {
        received_y_A_str[i] = UART0_Read();
    }
    BIGI_CALL(bigi_from_hex(received_y_A_str, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &received_y_A_bigi))
    PRINTF("- received y_A = 0x");
    bigi_print(&received_y_A_bigi);
    PRINTF("\n\n");
#else
    if ( NULL != fgets(received_y_A_str, (EXAMPLE_WORDLEN * sizeof(word_t) * 2) + 1, stdin)) {
        TIMERCALL(bigi_from_hex(received_y_A_str, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &received_y_A_bigi))
        PRINTF("- received y_A = 0x");
        bigi_print(&received_y_A_bigi);
        PRINTF("\n\n");
    }
    else {
        PRINTF("- ERROR OCCURED WHILE ENTERING THE NUMBER!\n\n");
    }
#endif
    
/* ########################################################################## */
    /* -------------------------------------------------------------------------
     *  compute Q = kB*A
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("compute Q = kB*A:\n");
    TIMERCALL(ecc_mult(&p_bigi, &a_bigi, &b_bigi, &rand_kB_bigi, &received_x_A_bigi, &received_y_A_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_Q = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_Q = 0x");
    bigi_print(&y_out_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  Cleanup
     *                                                                        */
#ifdef __MIKROC_PRO_FOR_ARM__
    /* nothing to clean, TODO: close IO? */
#else
    /* free allocated memory */
    free(received_x_A_str); 
    free(received_y_A_str);
    free(rand_kB_ary); 
    free(received_x_A_ary); 
    free(received_y_A_ary); 
    free(computed_x_B_ary); 
    free(computed_y_B_ary); 
    free(p_ary);
    free(a_ary);
    free(b_ary);
    free(x_P_ary);
    free(y_P_ary);
    free(O_ary);
    free(x_out_ary);
    free(y_out_ary);

#ifdef MONT_PART
    // free(p_mont_ary);
    free(a_mont_ary);
    free(b_mont_ary);
    free(x_P_mont_ary);
    free(y_P_mont_ary);
    free(x_Q_mont_ary);
    free(y_Q_mont_ary);
    free(x_out_mont_ary);
    free(y_out_mont_ary);
#endif /*MONT_PART*/
    
    for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
        free(tmpary[i].ary);
    free(tmpary);

    return 0;
#endif /* #ifdef __MIKROC_PRO_FOR_ARM__ */
}
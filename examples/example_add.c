#include <stdint.h>
#include <bigi.h>
#include <bigi_io.h>
#include <ecc.h>
#include <rng.h>

#ifndef __MIKROC_PRO_FOR_ARM__
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#endif

#define MONT_PART
#define EXAMPLE_BITLEN 79
#define EXAMPLE_WORDLEN ((EXAMPLE_BITLEN + MACHINE_WORD_BITLEN - 1) / (MACHINE_WORD_BITLEN))
#define EXAMPLE_TMPARY_LEN 20

#ifdef __MIKROC_PRO_FOR_ARM__
// TODO: adjust 79bits to 80 bits = 10B etc  = EXAMPLE_BITLEN + (MACHINE_WORD_BITLEN - 1)?  79 + 7 = 86 / 8 = 8 
word_t p_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t a_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t b_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_P_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_P_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_Q_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_Q_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

word_t O_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

word_t x_out_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_out_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];

#ifdef MONT_PART
// word_t p_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t a_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t b_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_P_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_P_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_Q_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_Q_mont_ary   [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t r_mont_ary     [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t x_out_mont_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t y_out_mont_ary [(EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
word_t mu_mont = 0;
#endif /*MONT_PART*/

word_t tmp_arys[EXAMPLE_TMPARY_LEN * (EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
bigi_t tmpary[EXAMPLE_TMPARY_LEN];
#endif /* ifdef __MIKROC_PRO_FOR_ARM__ */

#ifdef __MIKROC_PRO_FOR_ARM__
    #define PRINTF(BUFFER) UART0_Write_Text(BUFFER)
    #define CLOCKS_PER_SEC 48000 /* TODO: check */
    volatile uint64_t ISR_COUNT = 0;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    #define PRINTF(BUFFER) printf("%s", BUFFER)
    clock_t t_start;
#endif /* ifdef __MIKROC_PRO_FOR_ARM__ */

#ifdef DBGT
    #define TIMERCALL(func) { \
                                TimerStart(); \
                                BIGI_ST_VAR = func; \
                                ticks = TimerGet(); \
                                if (BIGI_ST_VAR != OK) \
                                { \
                                    PRINTF("\nraise \"Error code: "); \
                                    PRINTF(bigi_status_str(BIGI_ST_VAR)); \
                                    PRINTF("\"\n# "); \
                                } \
                            }
#else
    #define TIMERCALL(func) { \
                                TimerStart(); \
                                func; \
                                ticks = TimerGet(); \
                            }
#endif

void TimerStart()
{
#ifdef __MIKROC_PRO_FOR_ARM__
    word_t i;

    ISR_COUNT = 0;
    EC_REG_BANK_INST_INTERRUPT_CONTROL &= ~1l;
    INTS_INST_GIRQ23_SRC = 16;
    INTS_INST_GIRQ23_EN_CLR = 16;
    INTS_INST_GIRQ23_EN_SET = 16;
    INTS_INST_BLOCK_ENABLE_SET = 1l << 23;
    EnableInterrupts();
    NVIC_IntEnable(IVT_INT_GIRQ23);
    TIMER4_INST_CONTROL = 0x10;

    for(i = 0; i < 100; i++) asm nop; // soft delay

    TIMER4_INST_PRE_LOAD = 48000;
    TIMER4_INST_INT_EN  = 1;
    TIMER4_INST_CONTROL = 0x00000029;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    t_start = clock();
#endif
}

uint64_t TimerGet()
{
    uint64_t t_res;

#ifdef __MIKROC_PRO_FOR_ARM__
    DisableInterrupts();
    t_res = ISR_COUNT;
    ISR_COUNT = 0;
#else /* ifdef __MIKROC_PRO_FOR_ARM__ */
    clock_t t_end;
    t_end = clock();
    t_res = (uint64_t)(t_end - t_start);
#endif

    return t_res;
}

#ifdef __MIKROC_PRO_FOR_ARM__

void TimerInterrupt() iv IVT_INT_GIRQ23 ics ICS_AUTO
{
    if (INTS_INST_GIRQ23_EN_SET & (1ul << 4))
    {
        INTS_INST_GIRQ23_SRC |= 1ul << 4;
        ISR_COUNT++;
    }
}

#endif

#ifdef __MIKROC_PRO_FOR_ARM__
void main()
#else
int main(int argc, char ** argv)
#endif
{
    /* values from https://www.certicom.com/content/dam/certicom/images/pdfs/challenge-2009.pdf */
#if EXAMPLE_BITLEN == 79
    char * p   = "000062CE5177412ACA899CF5";
    char * a   = "000039C95E6DDDB1BC45733C";
    char * b   = "00001F16D880E89D5A1C0ED1";

    char * x_P = "0000315D4B201C208475057D";
    char * y_P = "0000035F3DF5AB370252450A";
    char * x_Q = "00000679834CEFB7215DC365";
    char * y_Q = "00004084BC50388C4E6FDFAB";
#elif EXAMPLE_BITLEN == 239
    char * p   = "00007CFB4C973A86CDAF898231E4960ACDBBF5B6A9017DBED75FFABDD892085D";
    char * a   = "000076D4219CF7498B5B471E85BC4DABA3CE47ADC806228FBB0BCE197C4F4556";
    char * b   = "00004F0911A649B98CD0D3F695695E44743EA948E70B78CAB2C24C4E7D50E2B3";
    char * x_P = "00000D35ED464403B23CC681F18534C14B6FA2ADE7720523F5094AD9BFBE4752";
    char * y_P = "000052F1BC7C3C7438A91099FDD53666A0185FB59688CA3E380840903B589BEB";
    char * x_Q = "00002193DCEAE32BC6EF61653DE4F1A141C15A9A6A1A7296802A887EBC0C7667";
    char * y_Q = "000064297E89EE340CFF78A531998CC3F3376AFD3AE177DBE30B82C93045F79D";
#else
    #error "Unknown example for given bigi bit-length."
#endif

    /* declare variables */
    uint64_t ticks;
#define BUFFLEN ((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN + 3) * HEXCHARS_PER_MW)
    char buff[BUFFLEN];

    index_t i;
    DECL_BIGI_STATUS

#ifndef __MIKROC_PRO_FOR_ARM__
    word_t *const p_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const a_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const b_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const x_P_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_P_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const x_Q_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_Q_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const O_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

    word_t *const x_out_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_out_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));

#ifdef MONT_PART
    // word_t *const p_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const a_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const b_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_P_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_P_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_Q_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_Q_mont_ary   = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const r_mont_ary     = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const x_out_mont_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t *const y_out_mont_ary = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
    word_t mu_mont = 0;
#endif /*MONT_PART*/

    bigi_t *const tmpary  = malloc(EXAMPLE_TMPARY_LEN * sizeof(bigi_t));
#endif /* #ifndef __MIKROC_PRO_FOR_ARM__ */

    bigi_t p_bigi, a_bigi, b_bigi, x_P_bigi, y_P_bigi, x_Q_bigi, y_Q_bigi;

    bigi_t x_O_bigi, y_O_bigi;

    bigi_t x_out_bigi, y_out_bigi;
#ifdef MONT_PART
    bigi_t /*p_mont_bigi,*/ a_mont_bigi, b_mont_bigi, x_P_mont_bigi, y_P_mont_bigi, x_Q_mont_bigi, y_Q_mont_bigi, r_mont_bigi;
    bigi_t x_out_mont_bigi, y_out_mont_bigi;
#endif /*MONT_PART*/

#ifdef __MIKROC_PRO_FOR_ARM__
    UART0_Init(115200);
    Delay_ms(100);   // Wait for UART module to stabilize

    PRINTF("\n# -------\n");
#endif

    p_bigi.ary = p_ary;
    p_bigi.wordlen = EXAMPLE_WORDLEN;
    p_bigi.domain = DOMAIN_NORMAL;

    a_bigi.ary = a_ary;
    a_bigi.wordlen = EXAMPLE_WORDLEN;
    a_bigi.domain = DOMAIN_NORMAL;

    b_bigi.ary = b_ary;
    b_bigi.wordlen = EXAMPLE_WORDLEN;
    b_bigi.domain = DOMAIN_NORMAL;

    x_P_bigi.ary = x_P_ary;
    x_P_bigi.wordlen = EXAMPLE_WORDLEN;
    x_P_bigi.domain = DOMAIN_NORMAL;

    y_P_bigi.ary = y_P_ary;
    y_P_bigi.wordlen = EXAMPLE_WORDLEN;
    y_P_bigi.domain = DOMAIN_NORMAL;

    x_Q_bigi.ary = x_Q_ary;
    x_Q_bigi.wordlen = EXAMPLE_WORDLEN;
    x_Q_bigi.domain = DOMAIN_NORMAL;

    y_Q_bigi.ary = y_Q_ary;
    y_Q_bigi.wordlen = EXAMPLE_WORDLEN;
    y_Q_bigi.domain = DOMAIN_NORMAL;

    x_O_bigi.ary = O_ary;
    x_O_bigi.wordlen = EXAMPLE_WORDLEN;
    x_O_bigi.domain = DOMAIN_NORMAL;

    y_O_bigi.ary = O_ary;
    y_O_bigi.wordlen = EXAMPLE_WORDLEN;
    y_O_bigi.domain = DOMAIN_NORMAL;

    x_out_bigi.ary = x_out_ary;
    x_out_bigi.wordlen = EXAMPLE_WORDLEN;
    x_out_bigi.domain = DOMAIN_NORMAL;

    y_out_bigi.ary = y_out_ary;
    y_out_bigi.wordlen = EXAMPLE_WORDLEN;
    y_out_bigi.domain = DOMAIN_NORMAL;

#ifdef MONT_PART

    // p_mont_bigi.ary = p_mont_ary;
    // p_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    // p_mont_bigi.domain = DOMAIN_MONTGOMERY;

    a_mont_bigi.ary = a_mont_ary;
    a_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    a_mont_bigi.domain = DOMAIN_MONTGOMERY;

    b_mont_bigi.ary = b_mont_ary;
    b_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    b_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_P_mont_bigi.ary = x_P_mont_ary;
    x_P_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_P_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_P_mont_bigi.ary = y_P_mont_ary;
    y_P_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_P_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_Q_mont_bigi.ary = x_Q_mont_ary;
    x_Q_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_Q_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_Q_mont_bigi.ary = y_Q_mont_ary;
    y_Q_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_Q_mont_bigi.domain = DOMAIN_MONTGOMERY;

    x_out_mont_bigi.ary = x_out_mont_ary;
    x_out_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    x_out_mont_bigi.domain = DOMAIN_MONTGOMERY;

    y_out_mont_bigi.ary = y_out_mont_ary;
    y_out_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    y_out_mont_bigi.domain = DOMAIN_MONTGOMERY;

    r_mont_bigi.ary = r_mont_ary;
    r_mont_bigi.wordlen = EXAMPLE_WORDLEN;
    r_mont_bigi.domain = DOMAIN_NORMAL;
#endif /*MONT_PART*/

    for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
    {
#ifdef __MIKROC_PRO_FOR_ARM__
        tmpary[i].ary      = &tmp_arys[i * (EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN)];
#else
        tmpary[i].ary      = malloc((EXAMPLE_WORDLEN + MULT_BUFFER_WORDLEN) * sizeof(word_t));
#endif
        tmpary[i].wordlen  = EXAMPLE_WORDLEN;
        tmpary[i].alloclen = EXAMPLE_WORDLEN;
        tmpary[i].domain   = DOMAIN_NORMAL;
    }

#ifdef DBGT
    PRINTF("---   DEBUG / TEST mode ON                            ---\n");
#else
    PRINTF("---   DEBUG / TEST mode OFF                            ---\n");
#endif
    sprintf(buff, "--    %u-bit numbers\n", EXAMPLE_BITLEN);
    PRINTF(buff);

    sprintf(buff, "--    %u-word numbers (alligned)\n", EXAMPLE_WORDLEN);
    PRINTF(buff);

    sprintf(buff, "--    %u-bit machine\n", MACHINE_WORD_BITLEN);
    PRINTF(buff);



    /* load from hex */
    PRINTF("# Loading numbers ...\n\n");
    TIMERCALL(bigi_from_hex(p, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &p_bigi))
    PRINTF("#  - loaded p\n");
    TIMERCALL(bigi_from_hex(a, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &a_bigi))
    PRINTF("#  - loaded a\n");
    TIMERCALL(bigi_from_hex(b, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &b_bigi))
    PRINTF("#  - loaded b\n");

    TIMERCALL(bigi_from_hex(x_P, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &x_P_bigi))
    PRINTF("#  - loaded x_P\n");
    TIMERCALL(bigi_from_hex(y_P, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &y_P_bigi))
    PRINTF("#  - loaded y_P\n");

    TIMERCALL(bigi_from_hex(x_Q, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &x_Q_bigi))
    PRINTF("#  - loaded x_Q\n");
    TIMERCALL(bigi_from_hex(y_Q, HEXCHARS_PER_MW * EXAMPLE_WORDLEN, &y_Q_bigi))
    PRINTF("#  - loaded y_Q\n");

    /* set point in infinity as O = [0, 0]*/
    TIMERCALL(bigi_set_zero(&x_O_bigi))
    TIMERCALL(bigi_set_zero(&y_O_bigi))
    PRINTF("#  - set  O = [0, 0]\n");
    /* -------------------------------------------------------------------------
     *  Init RNG - not needed now
     *                                                                        */
    // rng_init();

/* ########################################################################## */

    /* -------------------------------------------------------------------------
     *  x_P + y_P mod p
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("x_P + y_P % p:\n");
    TIMERCALL(mod_add(&x_P_bigi, &y_P_bigi, &p_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi))
    PRINTF(" - out = 0x");
    bigi_print(&x_out_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  x_Q + y_Q mod p
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("x_Q + y_Q % p:\n");
    TIMERCALL(mod_add(&x_Q_bigi, &y_Q_bigi, &p_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi))
    PRINTF(" - out = 0x");
    bigi_print(&x_out_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  x_P - x_Q mod p
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("x_P - x_Q  % p:\n");
    TIMERCALL(mod_sub(&x_P_bigi, &x_Q_bigi, &p_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi))
    PRINTF(" - out = 0x");
    bigi_print(&x_out_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);


    /* -------------------------------------------------------------------------
     *  y_P - y_Q mod p
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("y_P - y_Q % p:\n");
    TIMERCALL(mod_sub(&y_P_bigi, &y_Q_bigi, &p_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi))
    PRINTF(" - out = 0x");
    bigi_print(&x_out_bigi);

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n - (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  Add P + Q
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + Q:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_P_bigi, &y_P_bigi, &x_Q_bigi, &y_Q_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  Add Q + P
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("Q + P:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_Q_bigi, &y_Q_bigi, &x_P_bigi, &y_P_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);
    /* -------------------------------------------------------------------------
     *  Add P + O
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + O:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_P_bigi, &y_P_bigi, &x_O_bigi, &y_O_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);
    /* -------------------------------------------------------------------------
     *  Add O + Q
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("O + Q:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_O_bigi, &y_O_bigi, &x_Q_bigi, &y_Q_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);
    /* -------------------------------------------------------------------------
     *  Add O + O
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("O + O:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_O_bigi, &y_O_bigi, &x_O_bigi, &y_O_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);
    /* -------------------------------------------------------------------------
     *  Add P + P = 2P
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + P = 2P:\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_P_bigi, &y_P_bigi, &x_P_bigi, &y_P_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  Add P + (-P)
     *                                                                        */
    BIGI_CALL(bigi_sub(&p_bigi, &y_P_bigi, &tmpary[0]))
    
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + (-P):\n");
    TIMERCALL(ecc_add(&p_bigi, &a_bigi, &b_bigi, &x_P_bigi, &y_P_bigi, &x_P_bigi, &tmpary[0], &tmpary[1], EXAMPLE_TMPARY_LEN - 1, &x_out_bigi, &y_out_bigi))
    PRINTF(" - x_OUT = 0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT = 0x");
    bigi_print(&y_out_bigi);



#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "\n- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);


/* ########################################################################## */
#ifdef MONT_PART
    /* -------------------------------------------------------------------------
     *  Mont init
     *                                                                        */
    TimerStart();
    /* get Mont domain parameters */
    bigi_get_mont_params(&p_bigi, &tmpary[0], EXAMPLE_TMPARY_LEN, &r_mont_bigi, &mu_mont);
    #define MONT_PARAMS_READY

    /* convert numbers to Mont domain */
    bigi_to_mont_domain(&a_bigi, &p_bigi, &r_mont_bigi, &a_mont_bigi);
    bigi_to_mont_domain(&b_bigi, &p_bigi, &r_mont_bigi, &b_mont_bigi);
    bigi_to_mont_domain(&x_P_bigi, &p_bigi, &r_mont_bigi, &x_P_mont_bigi);
    bigi_to_mont_domain(&y_P_bigi, &p_bigi, &r_mont_bigi, &y_P_mont_bigi);
    bigi_to_mont_domain(&x_Q_bigi, &p_bigi, &r_mont_bigi, &x_Q_mont_bigi);
    bigi_to_mont_domain(&y_Q_bigi, &p_bigi, &r_mont_bigi, &y_Q_mont_bigi);

    ticks = TimerGet(); 
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("# ------------------------------------------------------------------------------\n\n");
    PRINTF("Calculations in Montgomery domain - INIT ");
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- (%10lu ms)\n\n", ticks);
#else
    sprintf(buff, "- (%10lu ms)\n\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    /* -------------------------------------------------------------------------
     *  P + Q in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + Q in Montgomery domain:\n");
    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_P_mont_bigi, &y_P_mont_bigi, &x_Q_mont_bigi, &y_Q_mont_bigi,
                           &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");

    /* -------------------------------------------------------------------------
     *  Q + P in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("Q + P in Montgomery domain:\n");
    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_Q_mont_bigi, &y_Q_mont_bigi, &x_P_mont_bigi, &y_P_mont_bigi,
                           &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");

    /* -------------------------------------------------------------------------
     *  P + O in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + O in Montgomery domain:\n");
    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_P_mont_bigi, &y_P_mont_bigi, &x_O_bigi, &y_O_bigi,
                           &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");

    /* -------------------------------------------------------------------------
     *  O + Q in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("O + P in Montgomery domain:\n");
    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_O_bigi, &y_O_bigi, &x_Q_mont_bigi, &y_Q_mont_bigi,
                           &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");

    /* -------------------------------------------------------------------------
     *  P + P  = 2P in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + P = 2P in Montgomery domain:\n");
    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_P_mont_bigi, &y_P_mont_bigi, &x_P_mont_bigi, &y_P_mont_bigi,
                           &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");

    /* -------------------------------------------------------------------------
     *  P + (-P) in Montgomery domain
     *                                                                        */
    PRINTF("# ------------------------------------------------------------------------------\n");
    PRINTF("P + (-P) in Montgomery domain:\n");
    BIGI_CALL(bigi_sub(&p_bigi, &y_P_mont_bigi, &tmpary[0]))

    TIMERCALL(ecc_add_mont(&p_bigi, &a_mont_bigi, &b_mont_bigi, 
                           &x_P_mont_bigi, &y_P_mont_bigi, &x_P_mont_bigi, &tmpary[0],
                           &r_mont_bigi, mu_mont, &tmpary[1], EXAMPLE_TMPARY_LEN - 1, 
                           &x_out_mont_bigi, &y_out_mont_bigi))
#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- calculation %10lu ms\n", ticks);
#else
    sprintf(buff, "- calculation %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    TimerStart();
    /* convert result from Montgomery domain */
    bigi_from_mont_domain(&x_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &x_out_bigi);
    bigi_from_mont_domain(&y_out_mont_bigi, &p_bigi, &r_mont_bigi, mu_mont, &tmpary[0], EXAMPLE_TMPARY_LEN, &y_out_bigi);
    ticks = TimerGet(); 

#ifdef __MIKROC_PRO_FOR_ARM__
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks);
#else
    sprintf(buff, "- convert from Mont %10lu ms\n", ticks * 1000/CLOCKS_PER_SEC);
#endif
    PRINTF(buff);

    PRINTF(" - x_mont_OUT = 0x");
    bigi_print(&x_out_mont_bigi);
    PRINTF("\n - y_mont_OUT = 0x");
    bigi_print(&y_out_mont_bigi);

    PRINTF("\n - x_OUT =      0x");
    bigi_print(&x_out_bigi);
    PRINTF("\n - y_OUT =      0x");
    bigi_print(&y_out_bigi);
    PRINTF("\n\n");
#endif /*MONT_PART*/
    /* -------------------------------------------------------------------------
     *  Cleanup
     *                                                                        */
#ifdef __MIKROC_PRO_FOR_ARM__
    /* nothing to clean, TODO: close IO? */
#else
    /* free allocated memory */
    free(p_ary);
    free(a_ary);
    free(b_ary);
    free(x_P_ary);
    free(y_P_ary);
    free(x_Q_ary);
    free(y_Q_ary);
    free(O_ary);
    free(x_out_ary);
    free(y_out_ary);

#ifdef MONT_PART
    // free(p_mont_ary);
    free(a_mont_ary);
    free(b_mont_ary);
    free(x_P_mont_ary);
    free(y_P_mont_ary);
    free(x_Q_mont_ary);
    free(y_Q_mont_ary);
    free(x_out_mont_ary);
    free(y_out_mont_ary);
#endif /*MONT_PART*/
    
    for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
        free(tmpary[i].ary);
    free(tmpary);

    return 0;
#endif /* #ifdef __MIKROC_PRO_FOR_ARM__ */
}
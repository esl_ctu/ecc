#!/bin/bash

reset
cd ../../bigi/
    ./build.sh -c
    cd ../ecc/
./build.sh -c

echo ""
echo ""
echo "    Running bigi examples"
echo "================================================================================"
echo ""

../bigi/bin/desktop_example | ruby

echo ""
echo ""
echo "    Running ECC examples"
echo "================================================================================"
echo ""

./bin/desktop_example

echo ""

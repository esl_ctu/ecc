#ifndef __MIKROC_PRO_FOR_ARM__
#include <time.h>
#endif

#include <bigi.h>
#include <rng.h>

volatile int init_done = 0;

void rng_init()
{
    if (init_done == 0)
    {
#ifdef __MIKROC_PRO_FOR_ARM__
        rng_power(1); /* power RNG block */
        rng_reset();  /* reset RNG */
        rng_mode(0);  /* set RNG to truly random mode */
        rng_start();  /* start filling 1024 bit FIFO */
#else
        srand(time(NULL));
#endif
        init_done = 1;
    }
}

index_t rng_get_bigi(bigi_t *const a,
                     const index_t words)
{
    index_t rng, i;

#ifdef DBGT
    if (words > a->wordlen)
        return 0;
#endif

    for (i = 0; i < a->wordlen + MULT_BUFFER_WORDLEN - words; i++)
        a->ary[i] = 0u;
#ifdef DEBUG
    for (i = 0; i < words; i++)
        a->ary[i] = 1u;
    rng = words;
#elif __MIKROC_PRO_FOR_ARM__
    rng = rng_get_words(&(a->ary[0]), words);
#else
    for (i = 0; i < words; i++)
        a->ary[i] = rand();
    rng = words;
#endif

    return rng;
}

void rng_rst()
{
#ifdef __MIKROC_PRO_FOR_ARM__
    rng_reset();
#endif
    init_done = 0;
}

#ifdef __MIKROC_PRO_FOR_ARM__
void rng_pause()
{
    rng_stop();
}
#endif

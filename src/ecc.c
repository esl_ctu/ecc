#include <bigi.h>
#include <stddef.h>
#include <ecc.h>
#include <bigi_io.h>

#define RUNNING_EXAMPLE

#ifndef __MIKROC_PRO_FOR_ARM__
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
#endif /* #ifndef __MIKROC_PRO_FOR_ARM__ */
#ifdef DBGT
    #include <bigi_io.h>
#endif

#ifdef __MIKROC_PRO_FOR_ARM__
    #include <bigi_io.h>
#endif /*__MIKROC_PRO_FOR_ARM__*/

BIGI_STATUS mod_add(const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const mod_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const out_bigi)
{

    BIGI_CALL(bigi_add(a_bigi, b_bigi, &TMPARY[0]))
    BIGI_CALL(bigi_mod_red(&TMPARY[0], mod_bigi, &TMPARY[1], tmparylen - 1, out_bigi))
    return OK;
}

BIGI_STATUS mod_sub(const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const mod_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const out_bigi)
{

    /* TODO check b < m */
    BIGI_CALL(bigi_sub(mod_bigi, b_bigi, &TMPARY[0]))
    BIGI_CALL(mod_add(a_bigi, &TMPARY[0], mod_bigi, &TMPARY[1], tmparylen - 1, out_bigi))
    return OK;
}

BIGI_STATUS ecc_add(const bigi_t *const p_bigi,
                    const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const x_P_bigi,
                    const bigi_t *const y_P_bigi,
                    const bigi_t *const x_Q_bigi,
                    const bigi_t *const y_Q_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const x_out_bigi,
                    bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_result_0 = NULL;
    bigi_t * tmp_result_1 = NULL;
    bigi_t * tmp_result_2 = NULL;
    bigi_t * lambda       = NULL;

    /* check if P == O */
    BIGI_CALL(bigi_is_zero(x_P_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_P_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /* copy Q to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_Q_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_Q_bigi))
            return OK;
        }
    }

    /* check if Q == O */
    BIGI_CALL(bigi_is_zero(x_Q_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /* copy P to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_P_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_P_bigi))
            return OK;
        }
    }

    tmp_result_0  = &TMPARY[0];
    tmp_result_1  = &TMPARY[1];
    tmp_result_2  = &TMPARY[2];
    lambda        = &TMPARY[3];
    /* check if x_P != x_Q */
    BIGI_CALL(bigi_cmp(x_P_bigi, x_Q_bigi, &cmp))
    if (cmp != CMP_EQUAL)
    {
        /*  lambda = (y_P – y_Q)/(x_P – x_Q) mod p */
        /*  > (y_P – y_Q) mod p */
        BIGI_CALL(mod_sub(y_P_bigi, y_Q_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_0))
        /*  > (x_P – x_Q) mod p */
        BIGI_CALL(mod_sub(x_P_bigi, x_Q_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_1))
        /*  > (x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_1, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_2))
        /*  > (y_P – y_Q)*(x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_mult(tmp_result_0, tmp_result_2, p_bigi, lambda))
    }
    else
    {
        /*check y_P != y_Q */
        BIGI_CALL(bigi_cmp(y_P_bigi, y_Q_bigi, &cmp))
        if (cmp != CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /*check y_Q == 0 */
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /* lambda = (3x_Q^2 + a)/(2y_Q) mod p */
        /* > (x_Q^2) mod p */
        BIGI_CALL(bigi_mod_mult(x_Q_bigi, x_Q_bigi, p_bigi, tmp_result_0))
        /* > 3(x_Q^2) mod p */
        BIGI_CALL(bigi_set_zero(tmp_result_1))
        tmp_result_1->ary[0] = 3;
        BIGI_CALL(bigi_mod_mult(tmp_result_1, tmp_result_0, p_bigi, tmp_result_2))
        /* > (3(x_Q^2) + a) mod p */
        BIGI_CALL(mod_add(tmp_result_2, a_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_0))
        /* > (2y_Q) mod p */
        BIGI_CALL(mod_add(y_Q_bigi, y_Q_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_1))
        /* > (2y_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_1, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_2))
        /* >                   (3x_Q^2 + a) * (2y_Q)^-1 mod   p  -> lambda */
        BIGI_CALL(bigi_mod_mult(tmp_result_0, tmp_result_2, p_bigi, lambda))
    }
    /* x_out = lambda^2 – x_P – x_Q mod p */
    /* > lambda^2 mod p */
    BIGI_CALL(bigi_mod_mult(lambda, lambda, p_bigi, tmp_result_0))
    /* > lambda^2 – x_P mod p */
    BIGI_CALL(mod_sub(tmp_result_0, x_P_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_1))
    /* > lambda^2 – x_P – x_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, x_Q_bigi, p_bigi, &TMPARY[4], tmparylen - 4, x_out_bigi))
    /* y_out = (x_Q – x_out)lambda – y_Q mod p */
    /* > (x_Q – x_out) mod p */
    BIGI_CALL(mod_sub(x_Q_bigi, x_out_bigi, p_bigi, &TMPARY[4], tmparylen - 4, tmp_result_0))
    /* > (x_Q – x_out)lambda mod p */
    BIGI_CALL(bigi_mod_mult(tmp_result_0, lambda, p_bigi, tmp_result_1))
    /* > (x_Q – x_out)lambda – y_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, y_Q_bigi, p_bigi, &TMPARY[4], tmparylen - 4, y_out_bigi))
    return OK;
}

BIGI_STATUS ecc_add_mont(const bigi_t *const p_bigi,
                         const bigi_t *const a_bigi,
                         const bigi_t *const b_bigi,
                         const bigi_t *const x_P_bigi,
                         const bigi_t *const y_P_bigi,
                         const bigi_t *const x_Q_bigi,
                         const bigi_t *const y_Q_bigi,
                         const bigi_t *const r_bigi,
                               word_t  const mu,
                               bigi_t *const TMPARY,
                         const index_t       tmparylen,
                               bigi_t *const x_out_bigi,
                    bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_result_0 = NULL;
    bigi_t * tmp_result_1 = NULL;
    bigi_t * tmp_result_2 = NULL;
    bigi_t * tmp_result_3 = NULL;
    bigi_t * lambda       = NULL;

    /*check if P == O */
    BIGI_CALL(bigi_is_zero(x_P_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_P_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /*copy Q to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_Q_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_Q_bigi))
            return OK;
        }
    }

    /*check if Q == O */
    BIGI_CALL(bigi_is_zero(x_Q_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /*copy P to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_P_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_P_bigi))
            return OK;
        }
    }

    tmp_result_0  = &TMPARY[0];
    tmp_result_1  = &TMPARY[1];
    tmp_result_2  = &TMPARY[2];
    tmp_result_3  = &TMPARY[3];
    lambda        = &TMPARY[4];
    /*check if x_P != x_Q */
    BIGI_CALL(bigi_cmp(x_P_bigi, x_Q_bigi, &cmp))
    if (cmp != CMP_EQUAL)
    {
        /* lambda = (y_P – y_Q)/(x_P – x_Q) mod p */
        /* > (y_P – y_Q) mod p */
        BIGI_CALL(mod_sub(y_P_bigi, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > (x_P – x_Q) mod p */
        BIGI_CALL(mod_sub(x_P_bigi, x_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
        /* > (x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_from_mont_domain(tmp_result_1, p_bigi, r_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_3))        
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_3, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_2))
        BIGI_CALL(bigi_to_mont_domain(tmp_result_2, p_bigi, r_bigi, tmp_result_3))
        /* > (y_P – y_Q)*(x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, tmp_result_3, p_bigi, mu, &TMPARY[5], tmparylen - 5, lambda))
    }
    else
    {
        /*check y_P != y_Q */
        BIGI_CALL(bigi_cmp(y_P_bigi, y_Q_bigi, &cmp))
        if (cmp != CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /*check y_Q == 0 */
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /* lambda = (3x_Q^2 + a)/(2y_Q) mod p */
        /* > (x_Q^2) mod p */
        BIGI_CALL(bigi_mod_mult_mont(x_Q_bigi, x_Q_bigi, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > 3(x_Q^2) mod p */
        BIGI_CALL(bigi_set_zero(tmp_result_3))
        tmp_result_3->ary[0] = 3;
        BIGI_CALL(bigi_to_mont_domain(tmp_result_3, p_bigi, r_bigi, tmp_result_1))
        /* > 3(x_Q^2) mod p  */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_1, tmp_result_0, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_2))
        /* > (3(x_Q^2) + a) mod p */
        BIGI_CALL(mod_add(tmp_result_2, a_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > (2y_Q) mod p */
        BIGI_CALL(mod_add(y_Q_bigi, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
        /* > (2y_Q)^-1 mod p */
        BIGI_CALL(bigi_from_mont_domain(tmp_result_1, p_bigi, r_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_3))
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_3, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_2))
        BIGI_CALL(bigi_to_mont_domain(tmp_result_2, p_bigi, r_bigi, tmp_result_3))
        /* >                        (3x_Q^2 + a) * (2y_Q)^-1 mod   p  -> lambda */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, tmp_result_3, p_bigi, mu, &TMPARY[5], tmparylen - 5, lambda))
    }
    /* x_out = lambda^2 – x_P – x_Q mod p */
    /* > lambda^2 mod p */
    BIGI_CALL(bigi_mod_mult_mont(lambda, lambda, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_0))
    /* > lambda^2 – x_P mod p */
    BIGI_CALL(mod_sub(tmp_result_0, x_P_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
    /* > lambda^2 – x_P – x_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, x_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, x_out_bigi))
    /* y_out = (x_Q – x_out)lambda – y_Q mod p */
    /* > (x_Q – x_out) mod p */
    BIGI_CALL(mod_sub(x_Q_bigi, x_out_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
    /* > (x_Q – x_out)lambda mod p */
    BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, lambda, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_1))
    /* > (x_Q – x_out)lambda – y_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, y_out_bigi))
    return OK;
}

BIGI_STATUS ecc_add_mont_fast(const bigi_t *const p_bigi,
                              const bigi_t *const a_bigi,
                              const bigi_t *const b_bigi,
                              const bigi_t *const x_P_bigi,
                              const bigi_t *const y_P_bigi,
                              const bigi_t *const x_Q_bigi,
                              const bigi_t *const y_Q_bigi,
                              const bigi_t *const r_bigi,
                              const bigi_t *const r3_bigi,
                                    word_t  const mu,
                                    bigi_t *const TMPARY,
                              const index_t       tmparylen,
                                    bigi_t *const x_out_bigi,
                                    bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_result_0 = NULL;
    bigi_t * tmp_result_1 = NULL;
    bigi_t * tmp_result_2 = NULL;
    bigi_t * tmp_result_3 = NULL;
    bigi_t * lambda       = NULL;

    /* check if P == O */
    BIGI_CALL(bigi_is_zero(x_P_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_P_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /* copy Q to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_Q_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_Q_bigi))
            return OK;
        }
    }

    /* check if Q == O */
    BIGI_CALL(bigi_is_zero(x_Q_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL)
        {
            /* copy P to OUT */
            BIGI_CALL(bigi_copy(x_out_bigi, x_P_bigi))
            BIGI_CALL(bigi_copy(y_out_bigi, y_P_bigi))
            return OK;
        }
    }

    tmp_result_0  = &TMPARY[0];
    tmp_result_1  = &TMPARY[1];
    tmp_result_2  = &TMPARY[2];
    tmp_result_3  = &TMPARY[3];
    lambda        = &TMPARY[4];
    /* check if x_P != x_Q */
    BIGI_CALL(bigi_cmp(x_P_bigi, x_Q_bigi, &cmp))
    if (cmp != CMP_EQUAL)
    {
        /* lambda = (y_P – y_Q)/(x_P – x_Q) mod p */
        /* > (y_P – y_Q) mod p */
        BIGI_CALL(mod_sub(y_P_bigi, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > (x_P – x_Q) mod p */
        BIGI_CALL(mod_sub(x_P_bigi, x_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
        /* > (x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_1, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_2))
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_2, r3_bigi, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_3))
        /* > (y_P – y_Q)*(x_P – x_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, tmp_result_3, p_bigi, mu, &TMPARY[5], tmparylen - 5, lambda))
    }
    else
    {
        /*check y_P != y_Q */
        BIGI_CALL(bigi_cmp(y_P_bigi, y_Q_bigi, &cmp))
        if (cmp != CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /*check y_Q == 0 */
        BIGI_CALL(bigi_is_zero(y_Q_bigi, &cmp))
        if (cmp == CMP_EQUAL) 
        {
            BIGI_CALL(bigi_set_zero(x_out_bigi))
            BIGI_CALL(bigi_set_zero(y_out_bigi))
            return OK;
        }
        /* lambda = (3x_Q^2 + a)/(2y_Q) mod p */
        /* > (x_Q^2) mod p */
        BIGI_CALL(bigi_mod_mult_mont(x_Q_bigi, x_Q_bigi, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > 3(x_Q^2) mod p */
        BIGI_CALL(bigi_set_zero(tmp_result_3))
        tmp_result_3->ary[0] = 3;
        BIGI_CALL(bigi_to_mont_domain(tmp_result_3, p_bigi, r_bigi, tmp_result_1))
        /* > (3(x_Q^2)  */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_1, tmp_result_0, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_2))
        /* > (3(x_Q^2) + a) mod p */
        BIGI_CALL(mod_add(tmp_result_2, a_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
        /* > (2y_Q) mod p */
        BIGI_CALL(mod_add(y_Q_bigi, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
        /* > (2y_Q)^-1 mod p */
        BIGI_CALL(bigi_mod_inv_binary(tmp_result_1, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_2))
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_2, r3_bigi, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_3))

        /* >                        (3x_Q^2 + a) * (2y_Q)^-1 mod   p  -> lambda */
        BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, tmp_result_3, p_bigi, mu, &TMPARY[5], tmparylen - 5, lambda))
    }
    /* x_out = lambda^2 – x_P – x_Q mod p */
    /* > lambda^2 mod p */
    BIGI_CALL(bigi_mod_mult_mont(lambda, lambda, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_0))
    /* > lambda^2 – x_P mod p */
    BIGI_CALL(mod_sub(tmp_result_0, x_P_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_1))
    /* > lambda^2 – x_P – x_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, x_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, x_out_bigi))
    /* y_out = (x_Q – x_out)lambda – y_Q mod p */
    /* > (x_Q – x_out) mod p */
    BIGI_CALL(mod_sub(x_Q_bigi, x_out_bigi, p_bigi, &TMPARY[5], tmparylen - 5, tmp_result_0))
    /* > (x_Q – x_out)lambda mod p */
    BIGI_CALL(bigi_mod_mult_mont(tmp_result_0, lambda, p_bigi, mu, &TMPARY[5], tmparylen - 5, tmp_result_1))
    /* > (x_Q – x_out)lambda – y_Q mod p */
    BIGI_CALL(mod_sub(tmp_result_1, y_Q_bigi, p_bigi, &TMPARY[5], tmparylen - 5, y_out_bigi))
    return OK;
}

BIGI_STATUS ecc_mult(const bigi_t *const p_bigi,
                     const bigi_t *const a_bigi,
                     const bigi_t *const b_bigi,
                     const bigi_t *const M_bigi,                    
                     const bigi_t *const x_P_bigi,
                     const bigi_t *const y_P_bigi,
                           bigi_t *const TMPARY,
                     const index_t tmparylen,
                           bigi_t *const x_out_bigi,
                           bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_x_result = NULL;
    bigi_t * tmp_y_result = NULL;
    int16_t i = 0;
    word_t readbit = 0u;
    /* OUT = [O,O] */
    BIGI_CALL(bigi_set_zero(x_out_bigi))
    BIGI_CALL(bigi_set_zero(y_out_bigi))

    /*check if multiplier is 0 */
    BIGI_CALL(bigi_is_zero(M_bigi, &cmp))
    if (cmp == CMP_EQUAL)
    {
        return OK;
    }

    tmp_x_result  = &TMPARY[0];
    tmp_y_result  = &TMPARY[1];
    /* find MSB bite */
    i = M_bigi->wordlen * MACHINE_WORD_BITLEN - 1;
    for (; i >= 0 ; --i)
    {
        BIGI_CALL(bigi_get_bit(M_bigi, i, &readbit))
        if (readbit)
            break;
    }
    /* down to 0 bit */
    for (; i >= 0 ; --i)
    {
        BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, x_out_bigi, y_out_bigi, x_out_bigi, y_out_bigi, &TMPARY[2], tmparylen - 2, tmp_x_result, tmp_y_result))
        BIGI_CALL(bigi_get_bit(M_bigi, i, &readbit))
        /* if bit == 1 -> OUT + P */
        if (readbit)
        {
            BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, tmp_x_result, tmp_y_result, x_P_bigi, y_P_bigi, &TMPARY[2], tmparylen - 2, x_out_bigi, y_out_bigi))
        }
        else
        {
            BIGI_CALL(bigi_copy(x_out_bigi, tmp_x_result))
            BIGI_CALL(bigi_copy(y_out_bigi, tmp_y_result))
        }
    }
    return OK;
}

BIGI_STATUS ecc_mult_mont_ladder (const bigi_t *const p_bigi,
                                  const bigi_t *const a_bigi,
                                  const bigi_t *const b_bigi,
                                  const bigi_t *const M_bigi,                    
                                  const bigi_t *const x_P_bigi,
                                  const bigi_t *const y_P_bigi,
                                        bigi_t *const TMPARY,
                                  const index_t tmparylen,
                                  const int16_t point_bitsize,
                                        bigi_t *const x_out_bigi,
                                        bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_x_result = NULL;
    bigi_t * tmp_y_result = NULL;
    bigi_t * tmp2_x_result = NULL;
    bigi_t * tmp2_y_result = NULL;
    int16_t i = 0;
    word_t readbit = 0u;
    /* OUT = [O,O] */
    BIGI_CALL(bigi_set_zero(x_out_bigi))
    BIGI_CALL(bigi_set_zero(y_out_bigi))

    /* check if multiplier is 0 */
    BIGI_CALL(bigi_is_zero(M_bigi, &cmp))
    if (cmp == CMP_EQUAL)
        return OK;

    tmp_x_result  = &TMPARY[0];
    tmp_y_result  = &TMPARY[1];

    tmp2_x_result  = &TMPARY[2];
    tmp2_y_result  = &TMPARY[3];

    BIGI_CALL(bigi_copy(tmp_x_result, x_P_bigi))
    BIGI_CALL(bigi_copy(tmp_y_result, y_P_bigi))

    for (i = point_bitsize - 1; i >= 0 ; --i)
    {
        BIGI_CALL(bigi_get_bit(M_bigi, i, &readbit))
        if (readbit)
        {
            BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, x_out_bigi,   y_out_bigi,   tmp_x_result, tmp_y_result, &TMPARY[4], tmparylen - 4, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(x_out_bigi, tmp2_x_result))
            BIGI_CALL(bigi_copy(y_out_bigi, tmp2_y_result))
            BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, tmp_x_result, tmp_y_result, tmp_x_result, tmp_y_result, &TMPARY[4], tmparylen - 4, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(tmp_x_result, tmp2_x_result))
            BIGI_CALL(bigi_copy(tmp_y_result, tmp2_y_result))
        }
        else
        {
            BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, x_out_bigi, y_out_bigi, tmp_x_result, tmp_y_result, &TMPARY[4], tmparylen - 4, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(tmp_x_result, tmp2_x_result))
            BIGI_CALL(bigi_copy(tmp_y_result, tmp2_y_result))
            BIGI_CALL(ecc_add(p_bigi, a_bigi, b_bigi, x_out_bigi, y_out_bigi, x_out_bigi,   y_out_bigi,   &TMPARY[4], tmparylen - 4, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(x_out_bigi, tmp2_x_result))
            BIGI_CALL(bigi_copy(y_out_bigi, tmp2_y_result))
        }
    }
    return OK;
}

BIGI_STATUS ecc_mult_mont_ladder_mont_domain (const bigi_t *const p_bigi,
                                              const bigi_t *const a_bigi,
                                              const bigi_t *const b_bigi,
                                              const bigi_t *const M_bigi,                    
                                              const bigi_t *const x_P_bigi,
                                              const bigi_t *const y_P_bigi,
                                              const bigi_t *const r_bigi,
                                                    word_t  const mu,
                                                    bigi_t *const TMPARY,
                                              const index_t       tmparylen,
                                              const int16_t       point_bitsize,
                                                    bigi_t *const x_out_bigi,
                                                    bigi_t *const y_out_bigi)
{
    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_x_result = NULL;
    bigi_t * tmp_y_result = NULL;
    bigi_t * tmp2_x_result = NULL;
    bigi_t * tmp2_y_result = NULL;
    bigi_t * r3_bigi = NULL;
    int16_t i = 0;
    word_t readbit = 0u;
    /* OUT = [O,O] */
    BIGI_CALL(bigi_set_zero(x_out_bigi))
    BIGI_CALL(bigi_set_zero(y_out_bigi))

    /* check if multiplier is 0 */
    BIGI_CALL(bigi_is_zero(M_bigi, &cmp))
    if (cmp == CMP_EQUAL)
        return OK;

    tmp_x_result  = &TMPARY[0];
    tmp_y_result  = &TMPARY[1];

    tmp2_x_result  = &TMPARY[2];
    tmp2_y_result  = &TMPARY[3];

    r3_bigi  = &TMPARY[4];
    BIGI_CALL(bigi_mod_mult(r_bigi, r_bigi, p_bigi, tmp_x_result))
    BIGI_CALL(bigi_mod_mult(tmp_x_result, r_bigi, p_bigi, r3_bigi))

    BIGI_CALL(bigi_copy(tmp_x_result, x_P_bigi))
    BIGI_CALL(bigi_copy(tmp_y_result, y_P_bigi))

    for (i = point_bitsize - 1; i >= 0 ; --i)
    {
        BIGI_CALL(bigi_get_bit(M_bigi, i, &readbit))
        if (readbit)
        {
            BIGI_CALL(ecc_add_mont_fast(p_bigi, a_bigi, b_bigi, x_out_bigi,   y_out_bigi,   tmp_x_result, tmp_y_result, r_bigi, r3_bigi, mu, &TMPARY[5], tmparylen - 5, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(x_out_bigi, tmp2_x_result))
            BIGI_CALL(bigi_copy(y_out_bigi, tmp2_y_result))
            BIGI_CALL(ecc_add_mont_fast(p_bigi, a_bigi, b_bigi, tmp_x_result, tmp_y_result, tmp_x_result, tmp_y_result, r_bigi, r3_bigi, mu, &TMPARY[5], tmparylen - 5, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(tmp_x_result, tmp2_x_result))
            BIGI_CALL(bigi_copy(tmp_y_result, tmp2_y_result))
        }
        else
        {
            BIGI_CALL(ecc_add_mont_fast(p_bigi, a_bigi, b_bigi, x_out_bigi, y_out_bigi, tmp_x_result, tmp_y_result, r_bigi, r3_bigi, mu, &TMPARY[5], tmparylen - 5, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(tmp_x_result, tmp2_x_result))
            BIGI_CALL(bigi_copy(tmp_y_result, tmp2_y_result))
            BIGI_CALL(ecc_add_mont_fast(p_bigi, a_bigi, b_bigi, x_out_bigi, y_out_bigi, x_out_bigi,   y_out_bigi,   r_bigi, r3_bigi, mu, &TMPARY[5], tmparylen - 5, tmp2_x_result, tmp2_y_result))
            BIGI_CALL(bigi_copy(x_out_bigi, tmp2_x_result))
            BIGI_CALL(bigi_copy(y_out_bigi, tmp2_y_result))
        }
    }
    return OK;
}
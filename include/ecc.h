#ifndef ECC_H
#define ECC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <bigi.h>
#include <rng.h>

/* -----------------------------------------------------------------------------
 *  Minimum required amount of temporary values
 *                                                                            */
#define PLAIN_ECC_INIT__MIN_TMPARY_LEN (2 + (MOD_INV__MIN_TMPARY_LEN))
#define PLAIN_ECC_ENCR__MIN_TMPARY_LEN (4 + (MODEXP_MONT__MIN_TMPARY_LEN))
#define PLAIN_ECC_DECR__MIN_TMPARY_LEN (2 + (MODEXP_MONT__MIN_TMPARY_LEN))

/**
 *  @brief          Modular addition
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS mod_add(const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const mod_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const out_bigi);


/**
 *  @brief          Modular substraction
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS mod_sub(const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const mod_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const out_bigi);


/**
 *  @brief          Add two points
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_add(const bigi_t *const p_bigi,
                    const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const x_P_bigi,
                    const bigi_t *const y_P_bigi,
                    const bigi_t *const x_Q_bigi,
                    const bigi_t *const y_Q_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const x_out_bigi,
                    bigi_t *const y_out_bigi);



/**
 *  @brief          Add two points in Mont domain
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_add_mont(const bigi_t *const p_bigi,
                    const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const x_P_bigi,
                    const bigi_t *const y_P_bigi,
                    const bigi_t *const x_Q_bigi,
                    const bigi_t *const y_Q_bigi,
                    const bigi_t *const r_bigi,
                    word_t const mu,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const x_out_bigi,
                    bigi_t *const y_out_bigi);


/**
 *  @brief          Add two points in Mont domain (using binary inversion a^-1*R = (a*R)^-1 * R^3 * R^-1 = mont_mult((a*R)^-1, R^3))
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_add_mont_fast(const bigi_t *const p_bigi,
                              const bigi_t *const a_bigi,
                              const bigi_t *const b_bigi,
                              const bigi_t *const x_P_bigi,
                              const bigi_t *const y_P_bigi,
                              const bigi_t *const x_Q_bigi,
                              const bigi_t *const y_Q_bigi,
                              const bigi_t *const r_bigi,
                              const bigi_t *const r3_bigi,
                                    word_t  const mu,
                                    bigi_t *const TMPARY,
                              const index_t       tmparylen,
                                    bigi_t *const x_out_bigi,
                                    bigi_t *const y_out_bigi);

/**
 *  @brief          Multiply point by a constatnt
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_mult(const bigi_t *const p_bigi,
                    const bigi_t *const a_bigi,
                    const bigi_t *const b_bigi,
                    const bigi_t *const M_bigi,                    
                    const bigi_t *const x_P_bigi,
                    const bigi_t *const y_P_bigi,
                    bigi_t *const TMPARY,
                    const index_t tmparylen,
                    bigi_t *const x_out_bigi,
                    bigi_t *const y_out_bigi);


/**
 *  @brief          Multiply point by a constatnt using Motgomery Ladder
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_mult_mont_ladder (const bigi_t *const p_bigi,
                                  const bigi_t *const a_bigi,
                                  const bigi_t *const b_bigi,
                                  const bigi_t *const M_bigi,                    
                                  const bigi_t *const x_P_bigi,
                                  const bigi_t *const y_P_bigi,
                                        bigi_t *const TMPARY,
                                  const index_t tmparylen,
                                  const int16_t point_bitsize,
                                        bigi_t *const x_out_bigi,
                                        bigi_t *const y_out_bigi);


/**
 *  @brief          Multiply point in Montgomery Domain by a constatnt using Motgomery Ladder
 *  @param[in]      
 *  @param[in]      
 *  @param[out]     
 *  @return         
 *
 */
BIGI_STATUS ecc_mult_mont_ladder_mont_domain (const bigi_t *const p_bigi,
                                              const bigi_t *const a_bigi,
                                              const bigi_t *const b_bigi,
                                              const bigi_t *const M_bigi,                    
                                              const bigi_t *const x_P_bigi,
                                              const bigi_t *const y_P_bigi,
                                              const bigi_t *const r_bigi,
                                                    word_t  const mu,
                                                    bigi_t *const TMPARY,
                                              const index_t       tmparylen,
                                              const int16_t       point_bitsize,
                                                    bigi_t *const x_out_bigi,
                                                    bigi_t *const y_out_bigi);                                          

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef ECC_H */
#ifndef RNG_H
#define RNG_H

#include <stdint.h>

#ifndef __MIKROC_PRO_FOR_ARM__
#include <stdlib.h>
#endif

#include <rng.h>

void rng_init();

index_t rng_get_bigi(bigi_t *const a,
                     const index_t words);

void rng_rst();

#ifdef __MIKROC_PRO_FOR_ARM__
void rng_pause();
#endif

#endif /* #ifndef RNG_H */
